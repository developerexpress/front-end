[![ForTheBadge built-with-love](http://ForTheBadge.com/images/badges/built-with-love.svg)](https://GitHub.com/Naereen/)
[![ForTheBadge makes-people-smile](http://ForTheBadge.com/images/badges/makes-people-smile.svg)](http://ForTheBadge.com)
[![made-for-VSCode](https://img.shields.io/badge/Made%20for-VSCode-1f425f.svg)](https://code.visualstudio.com/)


# **Front-end - Curso Developer Express**

Se você conhece um pouco sobre lógica de programação, design e está querendo mergulhar no mundo do front-end, aqui vamos nós!

Nosso foco aqui é a criação de páginas web de maneira que você tenha a base necessária para estudar todas as frameworks. 

---

## **Materiais de Apoio**

Esse material tem como objetivo apoiar e orientar todos os participantes do curso de developer express .

* URL API: https://backend-curso.azurewebsites.net/swagger/index.html


## **Links Úteis**

* [W3Schools Online Web](https://www.w3schools.com/)
* [MDN Web Dev Docs](https://developer.mozilla.org/pt-BR/)
* [Roadmap Front End](https://roadmap.sh/frontend)
* [Inspirações para Infográficos](https://jscharting.com/examples/chart-features/infographic/)
* [Inspirações para Portifólio](https://www.freecodecamp.org/news/15-web-developer-portfolios-to-inspire-you-137fb1743cae/)

## **Conteúdo**

Este repositório contém versões do projeto finalizado e versões do projeto com 

* projetoFinal: Projeto finalizado com css

* projetoFinalComBoostrap: Projeto final utilizando o framework web Bootstrap.

O conteúdo dos projetos de aula serão adicionados conforme as aulas forem ocorrendo.

* Aula1: Projeto realizado na aula 1.

* Aula2: Projeto realizado na aula 2.

* Aula3: Projeto realizado na aula 3.

* Aula4: Projeto realizado na aula 4.

* Aula5: Projeto realizado na aula 5.

### Tópicos abordados

* Profissão Desenvolvedor Front End 
* Construindo a casa: Estrutura (HTML). 
* Construindo a casa: Acabamentos (CSS Flexbox, Grid Layout, Media Queries) 
* Construindo a casa: Funcionalidades (JavaScript) 
* Bootstrap 
* Projeto final (Infográfico) 

### Conceitos

* Introdução a carreira front End 
* O que ele faz e qual seu papel no time de desenvolvimento? 
* Visão geral sobre ferramentas mais utilizadas 
* Apresentação de ferramentas utilizadas: Github, CodePen e Github1s 
* HTML 
* Estrutura Básica  
* Elementos 
* Atributos 
* Styles 
* Formato 
* CSS 
* Sintaxe 
* Seletores 
* Pseudoclasses 
* Media Queries 
* CSS FlexBox
* CSS Grid 
* Javascript 
* JS HTML DOM 
* Bootstrap 4  
* BS4 Template básico 
* Projeto Final 


## Protótipo

* [Zeplin - Visualização sem conta Zeplin](https://zpl.io/254XB78)



### Features
- [x] Arquivo ReadMe
- [x] Estrutura básica HTML
- [x] Estilização
- [x] Interações via Javascript
- [x] Bootstrap
- [x] Outros


### Demonstração 

🚧   🚀 Em construção...  🚧   


### Pré-requisitos

Antes de iniciar instale:

- Navegador de sua preferência. Recomendamos o [Google Chrome](https://chromeenterprise.google/intl/pt_br/browser/download/?utm_source=adwords&utm_medium=cpc&utm_campaign=2021-H1-chromebrowser-paidmed-paiddisplay-other-chromebrowserent&utm_term=downloadnow&gclid=Cj0KCQjwp86EBhD7ARIsAFkgakiBRmHW0UysRdsV9aHIE_HpAnFmrjh9OaPqtAO4sEkle6AT-CWZgaAaAsH_EALw_wcB&gclsrc=aw.ds#chrome-browser-download&utm_content=GCEJ) ou [Firefox Developer Edition](https://www.mozilla.org/pt-BR/firefox/developer/) 

- [VS Code](https://code.visualstudio.com/)

Antes de começar, você vai precisar ter instalado em sua máquina as seguintes ferramentas:
[Git](https://git-scm.com), ...

### 🎲 Baixando e editando

```bash
# Abrir Visual Studio

# Clicar no simbolo ⚠️ para abrir o terminal no visual studio

# Clone este repositório
$ git clone  endereço_do_repositorio

```


# Contatos 
Eduardo Sobrinho   
Jackson V. Oliveira  
Kátia Cibele 🙋‍♀️  
William Schanoski   




