
const baseUrl = 'https://backend-curso.azurewebsites.net'

window.onload = function () {
  getDeslocamento();
}

function mountContent(idDeslocamento, nomeCondutor, placaVeiculo, nomeCliente, inicioDeslocamento, fimDeslocamento, kmInicial, kmFinal) {
  var html = `<li key=${idDeslocamento} class="item_deslocamento">`
  html+= `
    <h2 class="identificacao_deslocamento">${nomeCondutor} - ${placaVeiculo}</h2>
    <div class="container_usuario_deslocamento container">
      <img
        class="usuario_deslocamento_img"
        src="./assets/person-circle.svg"
      />
      <h5 class="usuario_deslocamento_nome">${nomeCliente}</h5>
    </div>

    <div class="container_metricas_deslocamento container">

      <div
        class="container_data_inicio_deslocamento metricas_deslocamento"
      >
        <img class="img_data_inicio" src="./assets/saida.svg" />
        <p class="data_inicio_deslocamento">${formatData(inicioDeslocamento)}</p>
      </div>
      
      <div class="container_km_inicio_deslocamento metricas_deslocamento">
        <img class="img_km_inicio" src="./assets/velocimetro.svg" />
        <p class="km_inicio_deslocamento">${kmInicial} KM</p>
      </div>
      ${isIncludeKmFinal(kmFinal)}
    </div>
      ${isIncludeActionEncerrarDeslocamento(fimDeslocamento, kmFinal, idDeslocamento)}
  `
  html+='</li>'
  return html
}

function isIncludeKmFinal(kmFinal) {
  if(kmFinal === null) return ''
  return `
    <div class="container_km_fim_deslocamento metricas_deslocamento">
      <img class="img_km_fim" src="./assets/Chegada.svg" />
      <p class="km_fim_deslocamento">${kmFinal} KM</p>
    </div>
  `
}

function isIncludeActionEncerrarDeslocamento(fimDeslocamento, kmFinal, idDeslocamento) {
  if(kmFinal !== null && fimDeslocamento !== null) return '';
  return `
    <div class="container_encerrar_deslocamento">
      <img
        src="./assets/FlagInDeslocamento.svg"
        alt="Bandeira de fim de deslocamento"
        class="finalizar_deslocamento_img"
      />
      <button
        onclick="encerrarDeslocamento(${idDeslocamento}, this)"
        type="button"
        id="encerrar_deslocamento_btn"
        class="btn_secondary"
      >
        Encerrar Deslocamento
      </button>
  </div>
  
  `
}

function getDiaSemana(data) {
  const day = data.getDay();
  switch (day) {
    case 0: return 'DOM'
    case 1: return 'SEG'
    case 2: return 'TER'
    case 3: return 'QUA'
    case 4: return 'QUI'
    case 5: return 'SEX'
    default: return 'SAB'
  }

}

function formatData(data) {
  const newData = new Date(data)
  return getDiaSemana(newData) + " " + newData.getDate() + " " + (newData.getMonth() + 1) + " " +
  newData.getFullYear()+ " " + newData.getHours() + ":" + 
  (newData.getMinutes() < 10 ? `0${newData.getMinutes() }` : newData.getMinutes() ) 
}

function getDeslocamento() {
  fetch(`${baseUrl}/api/v1/Deslocamento`, {
    method: 'get'
  }).then(function(response) {
    response.json().then(function(data) {
      if(data.length > 0) {
        data.forEach(function(element, index){
          Promise.all([
            fetch(`${baseUrl}/api/v1/Condutor/${element.idCondutor}`),
            fetch(`${baseUrl}/api/v1/Cliente/${element.idCliente}`),
            fetch(`${baseUrl}/api/v1/veiculo/${element.idVeiculo}`)
          ]).then(async ([condutorRes, clienteRes, veiculoRes]) =>{
            const condutor = await condutorRes.json()
            const cliente = await  clienteRes.json()
            const veiculo = await veiculoRes.json()

            var contentLi = mountContent(element.id, condutor.nome, veiculo.placa, cliente.nome,
              element.inicioDeslocamento, element.fimDeslocamento, element.kmInicial, element.kmFinal)
            
            document.getElementById('content_deslocamentos').innerHTML += contentLi
          } )

        })
      }
    })

  }).catch(function(err) {
    console.error(err);

  })
}


function encerrarDeslocamento(idDeslocamento, element) {
  if(!element.disabled){
     element.disabled= true;

     fetch(`${baseUrl}/api/v1/Deslocamento/${idDeslocamento}/EncerrarDeslocamento`, {
       method: 'put',
       headers: {
         'Accept': 'application/json',
         'Content-type': 'application/json'
       },
       body: JSON.stringify({
          id: idDeslocamento,
          kmFinal: 100,
          fimDeslocamento: new Date(),
          observacao: ""      
       })
     }).then(function() {
       limparLista();
       getDeslocamento();
     }).catch(function(err){
        getDeslocamento();
        console.error(err)
     }).finally(() => { element.disabled=false;  });

  }
}

function limparLista() {
  var listLi = document.getElementById("content_deslocamentos");
  while(listLi.firstChild){
    listLi.removeChild(listLi.firstChild);
  }
}


// ------------

function filter() {
  var valueFilter = document.getElementById('search_input').value
  valueFilter = valueFilter.toUpperCase();
  ul = document.getElementById('content_deslocamentos');
  listLi = document.getElementsByTagName('li');

  for( i=0; i<listLi.length; i++) {
    txtValue = listLi[i].textContent;
    if(txtValue.toUpperCase().indexOf(valueFilter) > -1){
      listLi[i].style.display = 'block';
    }else {
      listLi[i].style.display = 'none';
    }
  }
}


