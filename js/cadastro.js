
const baseUrl = "https://backend-curso.azurewebsites.net"

window.onload = function() {
   setCliente();
   setCondutores();
   setVeiculos();
}

function setCliente() {
  fetch(`${baseUrl}/api/v1/Cliente`, {
    method: 'get'
  }).then( function(response) {
    response.json().then( function(data){
      let select = document.getElementById('cliente_select');
      data.map((item) => {
        const op = document.createElement('option');
        const node = document.createTextNode(item.nome);
        op.appendChild(node);
        op.value = item.id;
        select.appendChild(op);
      })
    })
  }).catch(function(err){
    alert(err);
  })

}


function setCondutores() {
  fetch(`${baseUrl}/api/v1/Condutor`, {
    method: 'get'
  }).then( function(response) {
    response.json().then( function(data){
      let select = document.getElementById('condutores_select');
      data.map((item) => {
        const op = document.createElement('option');
        const node = document.createTextNode(item.nome);
        op.appendChild(node);
        op.value = item.id;
        select.appendChild(op);
      })
    })
  }).catch(function(err){
    alert(err);
  })

}

function setVeiculos() {
  fetch(`${baseUrl}/api/v1/veiculo`, {
    method: 'get'
  }).then( function(response) {
    response.json().then( function(data){
      let select = document.getElementById('veiculo_select');
      data.map((item) => {
        const op = document.createElement('option');
        const node = document.createTextNode(item.placa);
        op.appendChild(node);
        op.value = item.id;
        select.appendChild(op);
      })
    })
  }).catch(function(err){
    alert(err);
  })

}

//  ----------------   mascaras

function maskData (element) {
  var v = element.value;
  v = v.replace(/\D/g, '');
  v = v.replace(/^(\d{2})(\d)/g, '$1/$2')
  v = v.replace(/(\d{2})\/(\d{2})(\d)/g, '$1/$2/$3')
  element.value = v;
}

function maskHora (val) {
  var v = val.value;
  v = v.replace(/\D/g, '');
  v = v.replace(/^(\d{2})(\d)/g, '$1:$2')
  val.value = v;
}

function justNumbers(element) {
  var v = element.value;
  v = v.replace(/[a-z]/g, '');
  element.value = v;
}

function getFieldsRef(){
  const inputDataPartida = document.getElementById('input_dataPartida');
  const inputHoraPartida = document.getElementById('input_horaPartida');
  const selectCondutores = document.getElementById('condutores_select');
  const selectVeiculo = document.getElementById('veiculo_select');
  const inputKmAtual = document.getElementById('input_kmAtual');
  const selectCliente = document.getElementById('cliente_select');
  const inputChecklist = document.getElementById('input_checklist');
  const inputMotivo = document.getElementById('input_motivo');
  const inputObs = document.getElementById('input_obs');

  return {
    inputDataPartida,
    inputHoraPartida,
    selectCondutores,
    selectVeiculo,
    inputKmAtual,
    selectCliente,
    inputChecklist,
    inputMotivo,
    inputObs,
  }
}

// ------  Retorna referencia dos erros
function getFieldsErrorRef() {
  const erroDataPartida = document.getElementById('erro_dataPartida');
  const erroHoraPartida = document.getElementById('erro_horaPartida');
  const erroCondutores = document.getElementById('erro_condutores');
  const erroVeiculo = document.getElementById('erro_veiculo');
  const erroKmAtual = document.getElementById('erro_kmAtual');
  const erroCliente = document.getElementById('erro_cliente');

  return {
    erroDataPartida,
    erroHoraPartida,
    erroCondutores,
    erroVeiculo,
    erroKmAtual,
    erroCliente,
  }
}


function validateFields() {
  const { inputDataPartida, inputHoraPartida } = getFieldsRef();
  var isValidDataPartida = validateDate(inputDataPartida)
  var isValidHoraPartida = validateTime(inputHoraPartida)
  var isValidFields = validateFieldsEmpty();


  return isValidDataPartida && isValidHoraPartida && isValidFields
}


function onClickGravarDeslocamento(event) {
  event.preventDefault();
  if(validateFields()){

    const { inputChecklist, inputDataPartida,inputHoraPartida , inputKmAtual,inputMotivo,inputObs, selectCliente, selectCondutores,selectVeiculo} = getFieldsRef()

    var dataSplit = inputDataPartida.value.split('/');
    var horaSplit = inputHoraPartida.value.split(':');
    var dataFormat = new Date(dataSplit[2], dataSplit[1] - 1, dataSplit[0], horaSplit[0], horaSplit[1]);

    fetch(`${baseUrl}/api/v1/Deslocamento/IniciarDeslocamento`, {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        kmInicial: inputKmAtual.value,
        inicioDeslocamento: dataFormat,
        checklist: inputChecklist.value,
        motivo: inputMotivo.value,
        observacao: inputObs.value,
        idCondutor: selectCondutores.value,
        idVeiculo: selectVeiculo.value,
        idCliente: selectCliente.value
      })
    }).then(function() {
      voltarListagem();
      //voltar a listagem
    }).catch(function(err) {
      voltarListagem();
    })
  }



}


// -------------- voltar a listagem 

function voltarListagem(event) {
  event?.preventDefault();
  var url = window.location.href;
  var newUrl = url.replace(/\/novo_deslocamento.html?/gm, "/index.html");
  window.location.replace(newUrl);
}


// --------------- validate date
function validateDate(element) {
  const {erroDataPartida} = getFieldsErrorRef();

  if(element.value === '') {
    erroDataPartida.textContent = "Informe o campo"
    erroDataPartida.style.display= "block";
    return false;
  } else {
    var dataSplit = element.value.split('/');
    if(dataSplit.length === 3) {
      let isValidDate = Date.parse(`${dataSplit[1]}/${dataSplit[0]}/${dataSplit[2]}`)
      if(isNaN(isValidDate)){
        erroDataPartida.textContent = "Informe uma data válida."
        erroDataPartida.style.display= "block";
        return false;
      }else {
        erroDataPartida.textContent = ""
        erroDataPartida.style.display= "none";
        return true;
      }

    }else {
      erroDataPartida.textContent = "Informe uma data válida."
      erroDataPartida.style.display= "block";
      return false;
    }
  }

}


function validateTime(element) {
  const { erroHoraPartida } = getFieldsErrorRef()
  if(element.value === ''){
    erroHoraPartida.textContent = "Informe o campo."
    erroHoraPartida.style.display = 'block';
    return false;
  }else {
    var horaSplit = element.value.split(':');

    if(horaSplit.length === 2){
      if(horaSplit[0] > 23 || horaSplit[1] > 59) {
        erroHoraPartida.textContent = "Informe um horário válido."
        erroHoraPartida.style.display = 'block';
        return false;
      } else {
        erroHoraPartida.textContent = ""
        erroHoraPartida.style.display = 'none';
        return true;
      }
    } else {
      erroHoraPartida.textContent = "Informe um horário válido."
      erroHoraPartida.style.display = 'block';
      return false;
    }
  }

}


function validateFieldsEmpty() {
  console.log('fdfdfd')
  const { selectCliente, selectCondutores, selectVeiculo, inputKmAtual } = getFieldsRef();
  const { erroCliente, erroCondutores, erroVeiculo, erroKmAtual} = getFieldsErrorRef()

  if( selectCliente.value !== '' && selectCondutores !== '' && selectVeiculo !== '' && inputKmAtual !== '') {
    erroCliente.style.display = 'none';
    erroCondutores.style.display = 'none';
    erroKmAtual.style.display = 'none';
    erroVeiculo.style.display = 'none';
    return true;
  }else {
    if( selectCliente.value === '' ) erroCliente.style.display = 'block';
    if( selectCondutores.value === '' ) erroCondutores.style.display = 'block';
    if( selectVeiculo.value === '' ) erroVeiculo.style.display = 'block';
    if( inputKmAtual.value === '' ) erroKmAtual.style.display = 'block';
    return false;
  }
}